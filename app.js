// https://github.com/alexscheelmeyer/node-phantom
var phantom  = require('node-phantom')
var io = require('socket.io').listen(8081);
var website = "http://google.com"
var async = require('async')



var q = async.queue(function(){}, 1)


io.sockets.on('connection', function(socket){
    console.log("there was a successful connection")
    socket.on('website', function(id, website){
        console.log('new website sent')

        console.log('adding to queue')
        q.push({'website': website}, grab_url_picture(id, website, function(ph){
            ph.exit();
        }))

    })
})


function grab_url_picture(id, website, callback){
    console.log('grab url called')
    phantom.create(function(err, ph) {
        console.log('New website created for: ' + website)

        return ph.createPage(function(err, page) {
            console.log('New page created for: ' + website)
            page.set('viewportSize', { width: 1080, height: 600 })
            page.set('clipRect', { top: 0, left: 0, width: 1080, height: 600 })

            return page.open(website , function(err, status) {
                page.includeJs('//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.1/jquery.min.js', function(err) {                
                    return page.evaluate(function(){
                        setTimeout(function(){
                            $("iframe").remove()
                        },2000)
                    }, function(err, result){
                        page.render('/Applications/MNPP/htdocs/MNPP/static/img/websites/' + id + '.png', function(err){
                            console.log("Successfully grabbed a web render of " + website)
                            callback(ph)
                        })  
                    })})
                ;
            });
        });
    });
}

process.on('uncaughtException', function (err) {
    console.log(err);
}); 
